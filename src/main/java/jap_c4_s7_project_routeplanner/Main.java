package jap_c4_s7_project_routeplanner;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

public class Main {
	
	int countRecords() {
		int cnt=0;
		try {
			BufferedReader br = new BufferedReader(new FileReader("routesInfo.csv"));
			while(br.readLine()!=null) {
				cnt++;
			}
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cnt;
	}
	
	int size = countRecords();
	RoutePlanner allRoutes[] = new RoutePlanner[size];
	
	void showAllRoutes() {
		
			try {
				BufferedReader br = new BufferedReader(new FileReader("routesInfo.csv"));
				String line;
				int i=0;
				while((line = br.readLine())!=null) {
					String values[] = line.split(",");
					allRoutes[i] = new RoutePlanner(values[0], values[1], values[2], values[3], values[4]);
					i++;
				}
				
				System.out.println("-------Showing All Routes Details-------");
				for(int j=0; j<allRoutes.length; j++) {
					System.out.println(allRoutes[j].getFromCity() + " " + allRoutes[j].getToCity() + " " 
				+ allRoutes[j].getDistance() + " " + allRoutes[j].getTime() + " " + allRoutes[j].getAirFare());
				}
			} 
			
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	void showDirectFlights(RoutePlanner allRoutes[], String fromCity) {
		Boolean flag = false;
		System.out.println("\n----Showing Direct flights from " + fromCity + "----");
		for(int i=0; i<allRoutes.length; i++) {
			if(allRoutes[i].getFromCity().equalsIgnoreCase(fromCity)) {
				flag = true;
				System.out.println(allRoutes[i].getFromCity() + " " + allRoutes[i].getToCity() + " " 
						+ allRoutes[i].getDistance() + " " + allRoutes[i].getTime() + " " + allRoutes[i].getAirFare());
			}
		}
		if(flag == false) {
			System.out.println("We are sorry. At this time, we don't have information on flights originating from " + fromCity);
		}
	}
	
	class SortByToCity implements Comparator<RoutePlanner> {
	    // Used for sorting in ascending order
	    public int compare(RoutePlanner a, RoutePlanner b)
	    {
	        return a.getToCity().compareTo(b.getToCity());
	    }
	    
	}
	void sortDirectFlights(RoutePlanner allRoutes[], String fromCity) {
		Arrays.sort(allRoutes, new SortByToCity());
		showDirectFlights(allRoutes, fromCity);
	}
	
	
	public void showAllConnections(RoutePlanner routes[], String fromCity, String toCity) {
        System.out.println("\n=====information about route======");
        RoutePlanner s[]=new RoutePlanner[5];
        int f=0;
        RoutePlanner s1[]=new RoutePlanner[5];
        int t=0;
        RoutePlanner s2[]=new RoutePlanner[5];
        int g=0;
        boolean flag=false;
        String viaCity="";
        
        for(int i=0;i<routes.length;i++){
            if(fromCity.equals(routes[i].getFromCity()) && toCity.equals(routes[i].getToCity())){
                flag=true;
                s[f]=routes[i];
                f++;
            }
            else if(fromCity.equals(routes[i].getFromCity())){
                s1[t]=routes[i];
                t++;
                viaCity = routes[i].getToCity();

            }
            else if(routes[i].getFromCity().equals(viaCity) && toCity.equals(routes[i].getToCity())){
                s2[g]=routes[i];
                g++;
                flag=true;
            }
        }
        if(flag==false)
        {
            System.out.println("there is no information about route");
        }
        for(int i=0;i<f;i++){
            System.out.println(s[i]);
        }
        for(int i=0;i<g;i++){
            System.out.println(s1[i]);
            System.out.println(s2[i]);
        }
    }
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Main m = new Main();
		m.showAllRoutes();
		m.showDirectFlights(m.allRoutes, "Delhi");
		m.sortDirectFlights(m.allRoutes, "Delhi");
		m.showDirectFlights(m.allRoutes, "Amsterdam");
		m.showAllConnections(m.allRoutes, "London", "Sydney");	
	}

}
