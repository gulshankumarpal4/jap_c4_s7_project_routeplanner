package jap_c4_s7_project_routeplanner;

public class RoutePlanner {
	
	private String fromCity;
	private String toCity;
	private String distance;
	private String time;
	private String airFare;
	
	public RoutePlanner(String fromCity, String toCity, String distance, String time, String airFare) {
		super();
		this.fromCity = fromCity;
		this.toCity = toCity;
		this.distance = distance;
		this.time = time;
		this.airFare = airFare;
	}
	public String getFromCity() {
		return fromCity;
	}
	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}
	public String getToCity() {
		return toCity;
	}
	public void setToCity(String toCity) {
		this.toCity = toCity;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getAirFare() {
		return airFare;
	}
	public void setAirFare(String airFare) {
		this.airFare = airFare;
	}
	
	@Override
	public String toString() {
		return "RoutePlanner [fromCity=" + fromCity + ", toCity=" + toCity + ", distance=" + distance + ", time=" + time
				+ ", airFare=" + airFare + "]";
	}

}
